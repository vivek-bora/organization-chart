import "@/styles/globals.scss";
import "@/styles/organization.scss";

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
