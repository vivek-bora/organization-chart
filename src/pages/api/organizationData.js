import organizationChartData from "../../../constants/data";

export default function organizationChart(req, res) {
  res.status(200).json({
    status: 200,
    message: "Successfully fetched organization data",
    data: organizationChartData,
  });
}
