import Head from "next/head";
import Image from "next/image";
import { Inter } from "next/font/google";
import styles from "@/styles/Home.module.css";
import { useEffect, useMemo, useState } from "react";
import Tag from "@/components/Tag";

const inter = Inter({ subsets: ["latin"] });

/**
 * Renders an employee card component to display employee
 * details and any direct reports.
 *
 * @param {Object} employee - The employee object with details to display.
 * @param {Array} employeesList - Full list of employees in org.
 */
export const EmployeeCard = ({ employee, employeesList, level, setLevel }) => {
  /**
   * Filters the employees list to find child employees
   * that directly report to the specified employee.
   */

  const childEmployees = useMemo(() => {
    return employeesList?.filter((eList) => {
      return eList.reporting_manager == employee.email;
    });
  }, [employee, employeesList]);

  return (
    <li
      className={`employeeCardWrapper ${
        childEmployees?.length > 0 ? "hasChildrens" : ""
      }`}
    >
      <div className="parentElement">
        <div className="profilePic">
          <Image src={"./userIcon.svg"} alt={""} width={50} height={50} />
        </div>
        <div>{employee.name}</div>
        <div className="employeeDetail">
          <Tag name={"ROLE"} />
          <div className="roleValue">{employee.designation}</div>
        </div>
        {/* <div className="employeeDetail">
          <Tag name={"Email"} />
          <div className="roleValue">{employee.email}</div>
        </div>
        <div className="employeeDetail">
          <Tag name={"Manager"} />
          <div className="roleValue">{employee.reporting_manager}</div>
        </div> */}
        <div className="employeeDetail">
          <Tag name={"LEVEL"} />
          <div className="level">{level}</div>
        </div>
      </div>
      {childEmployees && childEmployees.length > 0 && (
        <ul className="childElements">
          {childEmployees?.map((emp) => {
            let currentLevel = level + 1;

            return (
              <EmployeeCard
                employee={emp}
                key={emp.email}
                employeesList={employeesList}
                level={currentLevel}
                setLevel={setLevel}
              />
            );
          })}
        </ul>
      )}
    </li>
  );
};

/**
 * Home page component.
 * Fetches organization data from API on mount.
 * Renders employee cards for top-level employees.
 */
export default function Home() {
  const [organizationData, setOrganizationData] = useState();
  const [loading, setLoading] = useState(false);
  const [level, setLevel] = useState(1);

  /**
   * Filters the organization data to get only the top level employees (those without a reporting manager).
   */
  const topLevelEmployees = organizationData?.filter(
    (el) => el.reporting_manager == null
  );

  useEffect(() => {
    /**
     * Fetches organization data from API.
     */
    async function getOrganizationData() {
      setLoading(true);
      let response = await fetch("/api/organizationData");
      let responseData = await response.json();
      if (responseData.status == 200) {
        setOrganizationData(responseData.data);
        setLoading(false);
      }
    }

    getOrganizationData();
  }, []);

  if (loading) {
    return (
      <div className="loadingWrapper">
        <h3>Loading...</h3>
      </div>
    );
  }

  return (
    <>
      <Head>
        <title>Organization Chart by Vivek Bora</title>
        <meta name="description" content="Organization Chart by Vivek Bora" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header>
        <h1 className="header">Organization Chart by Vivek Bora</h1>
      </header>
      <main className={`${styles.main} ${inter.className} tree`}>
        <ul className="topLevelWrapper">
          {topLevelEmployees?.map((org) => {
            return (
              <EmployeeCard
                key={org.email}
                employee={org}
                employeesList={organizationData}
                level={level}
                setLevel={setLevel}
              />
            );
          })}
        </ul>
      </main>
    </>
  );
}
